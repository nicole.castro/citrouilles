// 20/01
var i = 0;
var explosed = 0;



var interval = setInterval(createPumpkin, 500);
/*var totalHeight = document.body.clientHeight; 
var totalWidth = document.body.clientWidth; */

const width  = window.innerWidth || document.documentElement.clientWidth || 
document.body.clientWidth;
const height = window.innerHeight|| document.documentElement.clientHeight|| 
document.body.clientHeight;

//console.log("h: "+height+"; w: "+width);

// TESTS 
/*for (var i = 0; i <= 1; i++) {
	createPumpkin();
}*/

function createPumpkin(){
	// AFFICHER COMPTEUR citrouilles explosées
	var c = document.createElement("div");
	c.className = "caca";
	c.innerHTML = explosed;
	document.body.appendChild(c);
	
	// s'il y a moins de 10 citrouilles
	if(document.getElementsByClassName("pk").length == 10){
		//console.log("fin");
		var end = document.createElement("div");
		end.className = "gameOver";
		end.innerHTML = "GAME OVER";
		document.body.appendChild(end); // ajouter au body
		clearInterval(interval); // arrêter la création de gameOver
		// interdire les clicks après fin du jeu
		document.addEventListener("click", handler, true);
	    function handler(e) {
	 		e.stopPropagation();
	  		e.preventDefault();
		}

	} else{

		/*pk.style.left = Math.random()*(height-300) + "px";
		pk.style.top = Math.random()*(width-300) + "px";*/
		
		/*var position = randomPosition();
		pk.style.left = position[0] + "px";
		pk.style.top = position[0] + "px";*/
		/*pk.style.left = 300px; // position fixée pour TESTS
		pk.style.top = 100px;*/

		// creation de variables de toutes les div pour une citrouille
		var pk = document.createElement("div");
		pk.className = "pk";
		// POSITION DES CITROUILLES
		pk.style.left = get_random() + "px";
		pk.style.top = get_random() + "px";
		// éléments de la citrouille
		var left_pumpkin = document.createElement("div");
		left_pumpkin.className = "left-pumpkin";
		var right_pumpkin = document.createElement("div");
		right_pumpkin.className = "right-pumpkin";
		var top_pumpkin = document.createElement("div");
		top_pumpkin.className = "top-pumpkin";
		var middle_pumpkin = document.createElement("div");
		middle_pumpkin.className = "middle-pumpkin";

		var mouth = document.createElement("div");
		mouth.className = "mouth";
		var hide_mouth = document.createElement("div");
		hide_mouth.className = "hide-mouth";
		var eye_left = document.createElement("div");
		eye_left.className = "eye-left";
		var eye_right = document.createElement("div");
		eye_right.className = "eye-right";
		var nose = document.createElement("div");
		nose.className = "nose";

		var teeth_up = document.createElement("div");
		teeth_up.className = "teeth-up";
		var teeth_down_left = document.createElement("div");
		teeth_down_left.className = "teeth-down-left";
		var teeth_down_right = document.createElement("div");
		teeth_down_right.className = "teeth-down-right";

		// ajouter les éléments à pk
		pk.appendChild(left_pumpkin);
		pk.appendChild(right_pumpkin);
		pk.appendChild(middle_pumpkin);
		pk.appendChild(top_pumpkin);

		pk.appendChild(mouth);
		pk.appendChild(hide_mouth);
		pk.appendChild(eye_left);
		pk.appendChild(eye_right);
		pk.appendChild(nose);

		pk.appendChild(teeth_up);
		pk.appendChild(teeth_down_left);
		pk.appendChild(teeth_down_right);

		document.body.appendChild(pk); // ajouter au body

		i += 1;
		console.log(i);

		pk.addEventListener("click", () => {
    		/*console.log("You knocked?");*/
 			explode(pk);
    			
    		destroyPumpkin(pk);
    		explosed++;
	});

	}
	
}

function destroyPumpkin(pk){
	/*pk.style.visibility = "none";*/
	pk.remove(); // supprime la citrouille sur laquelle on clique

}

function explode(pk, j){
	var explosion = document.createElement("div");
	explosion.className = "explosion";

	var r1 = document.createElement("div");
	r1.className = "r1";
	var r2 = document.createElement("div");
	r2.className = "r2";
	var r3 = document.createElement("div");
	r3.className = "r3";
	var r4 = document.createElement("div");
	r4.className = "r4";
	var r5 = document.createElement("div");
	r5.className = "r5";
	var r6 = document.createElement("div");
	r6.className = "r6";
	var r7 = document.createElement("div");
	r7.className = "r7";
	var r8 = document.createElement("div");
	r8.className = "r8";
	var r9 = document.createElement("div");
	r9.className = "r9";
	var r10 = document.createElement("div");
	r10.className = "r10";
	var r11 = document.createElement("div");
	r11.className = "r11";
	var r12 = document.createElement("div");
	r12.className = "r12";
	var r13 = document.createElement("div");
	r13.className = "r13";
	var r14 = document.createElement("div");
	r14.className = "r14";
	var r15 = document.createElement("div");
	r15.className = "r15";
	var r16 = document.createElement("div");
	r16.className = "r16";
	var r17 = document.createElement("div");
	r17.className = "r17";
	var r18 = document.createElement("div");
	r18.className = "r18";
	var r19 = document.createElement("div");
	r19.className = "r19";
	var r20 = document.createElement("div");
	r20.className = "r20";

	explosion.appendChild(r1);
	explosion.appendChild(r2);
	explosion.appendChild(r3);
	explosion.appendChild(r4);
	explosion.appendChild(r5);
	explosion.appendChild(r6);
	explosion.appendChild(r7);
	explosion.appendChild(r8);
	explosion.appendChild(r9);
	explosion.appendChild(r10);
	explosion.appendChild(r11);
	explosion.appendChild(r12);
	explosion.appendChild(r13);
	explosion.appendChild(r14);
	explosion.appendChild(r15);
	explosion.appendChild(r16);
	explosion.appendChild(r17);
	explosion.appendChild(r18);
	explosion.appendChild(r19);
	explosion.appendChild(r20);

	document.body.appendChild(explosion);

	//var as = pk.getBoundingClientRect(); // récupérer la position de la citrouille
	//explosion.style.left = parseInt(pk.style.left);
	//explosion.style.top = parseInt(pk.style.top);
	//var asx = parseInt(pk.style.left);
	//var asy = parseInt(pk.style.top);
	//explosion.style.left = asx + "px";
	//explosion.style.top = asy + "px";
	//var r1 = pk.getBoundingClientRect();
	/*var r1 = parseInt(pk.style.left);
	var r2 = pk.getBoundingClientRect();
	var x3 = r3.getBoundingClientRect();
	var x4 = r4.getBoundingClientRect();
	var x5 = r5.getBoundingClientRect();
	//console.log("pk :"+as.top, as.left);
	console.log("r1 :"+r1.top, r1.left);
	console.log("r2 :"+r2.top, r2.left);
	console.log("r3 :"+x3.top, x3.left);
	console.log("r4 :"+x4.top, x4.left);
	console.log("r5 :"+x5.top, x5.left);*/

	// récupérer la position de la citrouille
	var as = pk.getBoundingClientRect(); 
	explosion.style.left = as.left + "px";
	explosion.style.top = as.top + "px";


}

function randomPosition(){
	var x = Math.floor(Math.random() * document.body.clientWidth - 300);
	var y = Math.floor(Math.random() * document.body.clientHeight - 300);
	return [x,y];
}


function get_random(){
	var ranNum= Math.floor(Math.random()*500);
	return ranNum;
}











